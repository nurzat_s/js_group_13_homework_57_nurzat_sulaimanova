import { Component, ElementRef, ViewChild } from '@angular/core';
import { NewGroupService } from '../shared/new-group.service';
import { User } from '../shared/user.model';
import { GroupItem } from '../shared/group-item.model';

@Component({
  selector: 'app-new-group',
  templateUrl: './new-group.component.html',
  styleUrls: ['./new-group.component.css']
})
export class NewGroupComponent {
  @ViewChild('groupNameInput') groupNameInput!: ElementRef;

  constructor(private newGroupService: NewGroupService) {}
  users!:User[];

  createGroup() {
    const groupName = this.groupNameInput.nativeElement.value;

    const group = new GroupItem(groupName, false, this.users);
    this.newGroupService.addGroup(group);
  }
}
