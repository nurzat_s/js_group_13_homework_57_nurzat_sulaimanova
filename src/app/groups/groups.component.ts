import { Component, OnInit } from '@angular/core';
import { NewGroupService } from '../shared/new-group.service';
import { GroupItem } from '../shared/group-item.model';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {
  groups!: GroupItem[];
  constructor(private newGroupService: NewGroupService) {}

  ngOnInit() {
    this.groups = this.newGroupService.getGroups();
    this.newGroupService.groupsChange.subscribe((groups: GroupItem[]) => {
      this.groups = groups;
    })
  }
  focusedGroup = 0;

  // currentGroup() {
  //   this.focusedGroup = !this.focusedGroup;
  // }
  //
  // setInputClass(index: number) {
  //   this.focusedGroup = index;
  // }

}
