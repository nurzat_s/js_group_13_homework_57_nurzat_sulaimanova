import { Component, Input, OnInit } from '@angular/core';
import { NewGroupService } from '../../shared/new-group.service';
import { GroupItem } from '../../shared/group-item.model';
import { group } from '@angular/animations';

@Component({
  selector: 'app-group-item',
  templateUrl: './group-item.component.html',
  styleUrls: ['./group-item.component.css']
})
export class GroupItemComponent implements OnInit{
  @Input() group!: GroupItem;
  @Input() index!: number;
  groups!: GroupItem[];

  constructor(private newGroupService: NewGroupService, ) {}

  ngOnInit() {
    this.groups = this.newGroupService.getGroups();
    this.newGroupService.groupsChange.subscribe((groups: GroupItem[]) => {
      this.groups = groups;
    })
  }

  currentGroup(index: number)  {
    this.groups.forEach((group) => {
      if(group === this.groups[index]) {
        this.groups[index].focusedGroup = true;
      } else {
        group.focusedGroup = false;
      }
    })
  }

  setInputClass() {
    let className = '';
    if(this.groups[this.index].focusedGroup) {
      className = 'focus';
    }
    return className;
  }
}
