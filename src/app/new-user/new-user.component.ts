import { Component, ElementRef, EventEmitter, Output, ViewChild } from '@angular/core';
import { User } from '../shared/user.model';
import { UserService } from '../shared/user.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent {
  mode = 'yes';
  @ViewChild('nameInput') nameInput!: ElementRef;
  @ViewChild('emailInput') emailInput!: ElementRef;
  @ViewChild('modeInput') modeInput!: ElementRef;
  @ViewChild('selectInput') selectInput!: ElementRef;

  constructor(private userService: UserService) {}

  createUser() {
    const name = this.nameInput.nativeElement.value;
    const email = this.emailInput.nativeElement.value;
    const mode = this.modeInput.nativeElement.value;
    const select = this.selectInput.nativeElement.value;

    const user = new User(name, email, mode, select);
    this.userService.addUser(user);
  }

}
