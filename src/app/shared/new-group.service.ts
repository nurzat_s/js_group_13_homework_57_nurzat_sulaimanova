
import { EventEmitter } from '@angular/core';
import { User } from './user.model';
import { GroupItem } from './group-item.model';

export class NewGroupService {
  groupsChange = new EventEmitter<GroupItem[]>();
  users!:User[];

  private groups: GroupItem[] =[
   new GroupItem('Hiking club', false, this.users),
   new GroupItem('Booking club', false, this.users)
 ]

  getGroups() {
   return this.groups.slice();
  }

  addGroup(group: GroupItem) {
   this.groups.push(group);
   this.groupsChange.emit(this.groups);
  }
}
