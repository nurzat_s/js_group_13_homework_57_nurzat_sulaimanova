import { User } from './user.model';

export class GroupItem {
  constructor(
    public name: string,
    public focusedGroup: boolean,
    public user: User[],
  ) {}
}
