import { EventEmitter } from '@angular/core';
import { User } from './user.model';

export class UserService {
  usersChange = new EventEmitter<User[]>();
  private users: User[] = [
    new User('John', 'john@gmail.com', 'yes', 'user'),
    new User('Jane', 'jane@gmail.com', 'no', 'admin')
  ];

  getUsers() {
    return this.users.slice();
  }

  addUser(user: User) {
    this.users.push(user);
    this.usersChange.emit(this.users);
  }
}
