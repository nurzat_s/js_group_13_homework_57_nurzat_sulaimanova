import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NewUserComponent } from './new-user/new-user.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './users/user/user.component';
import { FormsModule } from '@angular/forms';
import { UserService } from './shared/user.service';
import { GroupsComponent } from './groups/groups.component';
import { GroupService } from './shared/group.service';
import { GroupItemComponent } from './groups/group-item/group-item.component';
import { NewGroupComponent } from './new-group/new-group.component';
import { NewGroupService } from './shared/new-group.service';


@NgModule({
  declarations: [
    AppComponent,
    NewUserComponent,
    UsersComponent,
    UserComponent,
    GroupsComponent,
    GroupItemComponent,
    NewGroupComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [UserService, GroupService, NewGroupService],
  bootstrap: [AppComponent]
})
export class AppModule { }
